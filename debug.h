#ifndef _DEBUG_H_
#define _DEBUG_H_
#pragma message ("Including support for Joe's debugging facilitities")
#include <cassert>

#ifdef _DEBUG
#ifndef _CONSOLE
#define _CONSOLE
#endif
#endif
/////////////////////////////////////////////////////////////////////////
//////////////// Various debugging helper functions /////////////////////
/////////////////////////////////////////////////////////////////////////

#define TO_STRING( expression )      (#expression)
#define APPEND( value1, value2 )     ( value1 ## value2 )
#define FILENAME                     (__FILE__)
#define LINE_NUMBER                  (__LINE__)

namespace Debug
{

	#ifdef _DEBUG
	bool initialize( const char *title = "[Debug Console]" );
	bool deinitialize( );

	#define confirmX( test ) if( !(test) ){\
			fprintf( stderr, "Test failed (%s) in %s, line %d!\n", TO_STRING(test), FILENAME, LINE_NUMBER );\
			abort( );}


	void panic( const char *message, ... );
	void panicIfFalse( bool test, const char *message, ... );
	void info( const char *message, ... );
	void print( const char *message, ... );
	#else
	inline void panic( const char *message, ... ){}
	inline void panicIfFalse( bool test, const char *message, ... ){}
	inline void info( const char *message, ... ){}
	inline void print( const char *message, ... ){}
	#endif // _DEBUG
} // end of namespace Debug



#endif // _DEBUG_H_
